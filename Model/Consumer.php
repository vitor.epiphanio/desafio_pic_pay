<?php


namespace App\Services;


use Jenssegers\Mongodb\Eloquent\Model;

class Consumer extends Model
{

    protected $table = 'consumers';

    protected $fillable = [
        'user_id',
        'username',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * Consumer constructor.
     */
    public function __construct()
    {
    }
}