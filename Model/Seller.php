<?php


namespace App\Services;



use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $table = 'sellers';

    protected $fillable = [
        'cnpj',
        'nome_fantasia',
        'nome_social',
        'user_id',
        'username',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * Seller constructor.
     */
    public function __construct()
    {
    }
}