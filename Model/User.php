<?php


namespace App\Services;



use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $fillable = [
        'cpf',
        'email',
        'nome_completo',
        'password',
        'numero_telefone',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'consumer',
        'seller',
    ];

    public function seller()
    {
        return $this->hasOne('App\Models\Seller');
    }

    public function consumer()
    {
        return $this->hasOne('App\Models\Consumer');
    }
}
