<?php

namespace App\Http\Controllers;

use App\Exceptions\ExceptionInputForm;
use App\Services\ConsumerServices;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class ConsumersController extends BaseController
{
    protected $ConsumerServices;

    public function __construct()
    {
        $this->ConsumerServices = new ConsumerServices();
    }

    public function store(Request $request)
    {
        try {

            return response($this->ConsumerServices->store($request), 201);

        } catch (ExceptionInputForm $e) {

            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        } catch (Exception $e) {

            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        }
    }
}
