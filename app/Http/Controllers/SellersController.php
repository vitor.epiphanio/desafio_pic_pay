<?php

namespace App\Http\Controllers;

use App\Exceptions\ExceptionInputForm;
use App\Services\SellerServices;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class SellersController extends BaseController
{

    protected $SellerServices;

    public function __construct()
    {
        $this->SellerServices = new SellerServices();
    }

    public function store(Request $request)
    {
        try {

            return response($this->SellerServices->store($request), 201);

        }  catch (QueryException $e) {

            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        } catch (RuntimeException $e) {

            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        } catch (Exception $e) {

            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        }
    }
 
}
