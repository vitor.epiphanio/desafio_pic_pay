<?php

namespace App\Http\Controllers;

use App\Exceptions\ExceptionInputForm;
use App\Exceptions\ExceptionNotFoundTransaction;
use App\Services\TransactionServices;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use RuntimeException;
use GuzzleHttp\Client;

    
class TransactionsController extends BaseController
{

    protected $TransactionServices;

    public function __construct()
    {
        $this->TransactionServices = new TransactionServices();
        $client = new Client();
    }


    public function store(Request $request)
    {
        try {

            return response($this->TransactionServices->store($request), 201);

        } catch (RuntimeException $e) {

            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        } catch (ExceptionInputForm $e) {

            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        } catch (Exception $e) {

            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        }
    }

    public function show($transaction_id)
    {
        try {

            return response($this->TransactionServices->show($transaction_id), 200);

        } catch (ExceptionNotFoundTransaction $e) {

            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        } catch (Exception $e) {

            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        } 
    }
   
}
