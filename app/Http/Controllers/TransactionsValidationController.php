<?php

namespace App\Http\Controllers;

use App\Exceptions\ExceptionInputForm;
use App\Exceptions\ExceptionNotFoundTransaction;
use App\Services\TransactionValidationServices;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use RuntimeException;


    
class TransactionsValidationController extends BaseController
{

    protected $TransactionValidationServices;

    public function __construct()
    {
        $this->TransactionValidationServices = new TransactionValidationServices();
    }


    public function store(Request $request)
    {

        try {

            $transaction = $this->TransactionValidationServices->store($request);
            return response()->json($transaction, 200);

        } catch (Exception $e) {

            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        }

    }
   
}
