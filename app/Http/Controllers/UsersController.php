<?php

namespace App\Http\Controllers;

use App\Services\UserServices;
use Illuminate\Http\Request;
use App\Exception\ExceptionServer;
use App\Exceptions\ExceptionNotFoundUser;
use App\Exceptions\ExceptionInputForm;
use Laravel\Lumen\Routing\Controller as BaseController;

class UsersController extends BaseController
{

    protected $UserServices;

    public function __construct()
    {
        $this->UserServices = new UserServices();
    }

    public function index()
    {
        try {

            return response($this->UserServices->index(), 200);

        } catch (ExceptionServer $e) {

            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        }
    }

    public function store(Request $request)
    {
        try {
            
            return response($this->UserServices->store($request), 201);

        } catch (ExceptionInputForm $e){
            
            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        } catch (Exception $e){
            
            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        }
    }

    public function show(Request $request)
    {
        try {

            return response($this->UserServices->show($request->query('q')), 200);

        } catch (ExceptionNotFoundUser $e){
            
            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        } catch (Exception $e){
            
            return response([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ], $e->getCode());

        }
    }
}
