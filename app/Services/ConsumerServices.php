<?php

namespace App\Services;

use App\Models\Consumer;
use App\Exceptions\ExceptionInputForm;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use RuntimeException;


class ConsumerServices
{
    public function store(Request $request)
    {
        try {
            
            $user_id = $request->input('user_id');
            $username = $request->input('username');

            $errors = [];

            if (empty($user_id)) $errors[] = 'UserID é obrigatório';
            if (empty($username)) $errors[] = 'Username é obrigatório';

            if (!empty($errors)) {

                throw new ExceptionInputForm(implode("\n", $errors), 422);
            }

            $newConsumer = new Consumer;
            $newConsumer['user_id'] = $user_id;
            $newConsumer['username'] = $username;
            $newConsumer->save();

            return ['data'=>$newConsumer,'status'=>'OK'];

        } catch (ExceptionInputForm $e) {
            
            throw new ExceptionInputForm($e->getMessage(), $e->getCode());
            
        } catch (Exception $e) {
            
            throw new Exception("Erro interno do servidor", 500);

        }
    }
}
