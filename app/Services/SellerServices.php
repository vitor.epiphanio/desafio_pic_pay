<?php

namespace App\Services;

use App\Exceptions\ExceptionInputForm;
use App\Models\Seller;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use RuntimeException;


class SellerServices
{
    public function store(Request $request)
    {
        try {
            $cnpj = $request->input('cnpj');
            $nome_fantasia = $request->input('fantasy_name');
            $nome_social = $request->input('social_name');
            $user_id = $request->input('user_id');
            $username = $request->input('username');

            $errors = [];

            if (empty($cnpj)) $errors[] = 'CNPJ é obrigatório';
            if (empty($nome_fantasia)) $errors[] = 'Nome Fantasia é obrigatório';
            if (empty($nome_social)) $errors[] = 'Razão Social é obrigatório';
            if (empty($user_id)) $errors[] = 'UserID é obrigatório';
            if (empty($username)) $errors[] = 'Username é obrigatório';

            if (!empty($errors)) {
                throw new ExceptionInputForm(implode("\n", $errors), 422);
            }

            $newSeller = new Seller;
            $newSeller['cnpj'] = $cnpj;
            $newSeller['fantasy_name'] = $nome_fantasia;
            $newSeller['social_name'] = $nome_social;
            $newSeller['user_id'] = $user_id;
            $newSeller['username'] = $username;
            $newSeller->save();

            return ['data'=>$newSeller,'status'=>'OK'];
            
        } catch (ExceptionInputForm $e) {
            
            throw new ExceptionInputForm($e->getMessage(), $e->getCode());
            
        } catch (Exception $e) {
            
            throw new Exception("Erro interno do servidor", 500);

        }

    }
}
