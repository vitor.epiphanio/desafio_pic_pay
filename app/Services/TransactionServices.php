<?php

namespace App\Services;

use App\Exceptions\ExceptionInputForm;
use App\Exceptions\ExceptionNotFoundTransaction;
use App\Models\Transaction;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use RuntimeException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;


class TransactionServices
{
    public function store(Request $request)
    {
        

        try {

            $payee_id = $request->input('payee_id');
            $payer_id = $request->input('payer_id');
            $value = $request->input('value');

            $errors = [];

            if (empty($payee_id)) $errors[] = 'Beneficiário é obrigatório';
            if (empty($payer_id)) $errors[] = 'Pagador obrigatório';
            if (empty($value)) $errors[] = 'Valor é obrigatório';

            $client = new \GuzzleHttp\Client();

            $response = $client->request('POST', '127.0.0.1/transactions/validation', [
                'form_params' => [
                    'valueTransaction' => $value,
                ]
            ]);
               
            $response = $response->getBody()->getContents();
            
            if ($response == 'false') {
                throw new RuntimeException("Identificamos que esta tentando fazer uma trasação abaixo de R$ 100.00, por enquanto esses tipos não são autorizadas. Por favor, tente novamente em breve fechou?.",401);
            }

            if (!empty($errors)) {
                throw new ExceptionInputForm(implode("\n", $errors), 422);
            }

            $newTransaction = new Transaction;
            $newTransaction->payee_id = $payee_id;
            $newTransaction->payer_id = $payer_id;
            $newTransaction->transaction_date = '2020-03-04 18:32:16';
            $newTransaction->value = $value;
            $newTransaction->save();

            return ['data'=>$newTransaction,'status'=>'OK'];


        } catch (RuntimeException $e) {
            
            throw new RuntimeException($e->getMessage(), $e->getCode());
            
        } catch (ExceptionInputForm $e) {
            
            throw new ExceptionInputForm($e->getMessage(), $e->getCode());
            
        }  catch (Exception $e) {

            throw new Exception ("Erro interno do servidor.", 500);

        } 

    }

    public function show($transaction_id)
    {
        try {

            $transaction = Transaction::find($transaction_id);
            
            if (!$transaction) {
            
                throw new ExceptionNotFoundTransaction("Transação não encontrada", 404);
            
            } 
            return $transaction;

        } catch (Exception $e) {

            throw new Exception ("Erro interno do servidor.", 500);

        } 
    }
}
