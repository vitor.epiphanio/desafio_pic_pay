<?php

namespace App\Services;

use App\Models\Transaction;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use GuzzleHttp\Client;


class TransactionValidationServices
{
    public function store(Request $request)
    {
        try {

            $status = true;

            if ($request->input('valueTransaction') > 100) $status = false;

            return $status;

        } catch (Exception $e) {

            throw new Exception ("Erro interno do servidor.", 500);

        } 
    }

}
