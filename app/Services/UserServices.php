<?php

namespace App\Services;

use App\Models\User;
use App\Exceptions\ExceptionServer;
use App\Exceptions\ExceptionNotFoundUser;
use App\Exceptions\ExceptionInputForm;
//use App\User;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use RuntimeException;
use DB;


class UserServices
{
    public function index()
    {
        try {

            return $users = User::all();
            
        } catch (ExceptionServer $e) {
            
            throw new ExceptionServer("Erro interno do servidor.", 500);
           
        }

    }

    public function store(Request $request)
    {
        try {

            $cpf = $request->input('cpf');
            $email = $request->input('email');
            $nome_completo = $request->input('nome_completo');
            $password = $request->input('password');
            $numero_telefone = $request->input('numero_telefone');

            $errors = [];

            if (empty($cpf)) $errors[] = 'CPF é obrigatório';
            if (empty($email)) $errors[] = 'E-mail obrigatório';
            else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) $errors[] = 'E-mail inválido';
            if (empty($nome_completo)) $errors[] = 'Nome Completo é obrigatório';
            if (empty($password)) $errors[] = 'Senha é obrigatório';
            if (empty($numero_telefone)) $errors[] = 'Telefone é obrigatório';

            if (!empty($errors)) {
                throw new ExceptionInputForm(implode("\n", $errors),422);
            }

            $newUser = new User;
            $newUser->cpf = $cpf;
            $newUser->email = $email;
            $newUser->nome_completo = $nome_completo;
            $newUser->password = $password;
            $newUser->numero_telefone = $numero_telefone;
            $newUser->save();

            return ['data'=>$newUser,'status'=>'OK'];

        } catch (ExceptionInputForm $e) {
        
            throw new ExceptionInputForm($e->getMessage(), $e->getCode());
        
        } catch (Exception $e) {
        
            throw new Exception("Erro interno no Sistema.", 500);
        
        } 

    }

    public function show($request)
    {
        try {

            $userName = $request;
 
            $user =  User::where('full_name', $userName)->first();
                           
            if (!$user) {
                throw new ExceptionNotFoundUser("Usuário não encontrado", 404);
            }

            $consumer = $user->consumer;
            $seller = $user->seller;

            $ret = [
                "accounts" => [],
                "user" => $user->toArray(),
            ];

            if ($consumer) $ret['accounts']['consumer'] = $consumer->toArray();
            if ($seller) $ret['accounts']['seller'] = $seller->toArray();

            return $ret;
            
        } catch (Exception $e) {

            throw new Exception("Erro interno do servidor", 500);
        
        }

    }
}
