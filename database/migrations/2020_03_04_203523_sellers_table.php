<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sellers');
        Schema::create('sellers', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('CNPJ', 250)->unique();
            $table->string('nome_socia', 250);
            $table->string('nome_fantasia', 250);
            $table->unsignedBigInteger('user_id');
            $table->string('usrname', 250)->unique();
            $table->timestamp();
            $table->softDeletesTz();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellers');
    }
}
