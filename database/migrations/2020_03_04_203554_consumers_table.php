<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConsumersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('consumers');
        Schema::create('consumers', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('username')->unique();
            $table->unsignedBigInteger('user_id');
            $table->timestampTz();
            $table->softDeletesTz();

            $table->foreign('user-id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumers');
    }
}
